﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomizeDoxygenComment
{
    public partial class CustomizeDoxygenComment : Form
    {
        const string SUMMARY = "<summary>";
        const string REMARKS = "<remarks>";
        const string SUMMARY_END = "</summary>";
        const string REMARKS_END = "</remarks>";
        const string SUMMARY_ADD = "__概要__  ";
        const string REMARKS_ADD = "__詳細__  ";

        // OpenFileDialog
        private OpenFileDialog ofd = new OpenFileDialog();

        public CustomizeDoxygenComment()
        {
            InitializeComponent();

            // ダイアログの初期化
            InitializeSelectDialog();
        }

        static void AddComment(string filepath)
        {
            List<string> data = new List<string>();
            string now, before;

            using (StreamReader sr = new StreamReader(filepath))
            {
                now = sr.ReadLine();
                data.Add(now);

                while (sr.Peek() > -1)
                {
                    before = now;
                    now = sr.ReadLine();
                    SubAddFunc(before, now, ref data);
                }
            }


            using (StreamWriter sw = new StreamWriter(filepath))
            {
                data.ForEach(l =>
                {
                    sw.WriteLine(l);
                });
            }
        }

        static void SubAddFunc(string before, string now, ref List<string> data)
        {
            string GetSpaceStr(string str) => str.Split("///".ToCharArray())[0];

            //if (new Regex("/^private| private /").IsMatch(now))
            //{
            //    data.Add(now);
            //}
            // <summary>
            if (before.Contains(SUMMARY) && !now.Contains(SUMMARY_ADD.Trim()) && IsComment(now) && !now.Contains(REMARKS))
            {
                var s = before.Replace(SUMMARY, SUMMARY_ADD);
                data.Add(s);
                data.Add(now);
                
            }
            // <summary> </summary>
            else if (now.Contains(SUMMARY) && now.Contains(SUMMARY_END) && IsComment(now))
            {
                string space = GetSpaceStr(now);
                string s = now.Replace(SUMMARY, "").Replace(SUMMARY_END, "");

                data.Add(space + "/// " + SUMMARY);
                data.Add(space + "/// " + SUMMARY_ADD);
                data.Add(s);
                data.Add(space + "/// " + SUMMARY_END);
            }
            // <remarks>
            else if (before.Contains(REMARKS) && !now.Contains(REMARKS_ADD.Trim()) && IsComment(now))
            {
                var s = before.Replace(REMARKS, REMARKS_ADD);
                data.Add(s);
                data.Add(now);
            }
            else
            {
                data.Add(now);
            }

        }

        static bool IsComment(string s) => s.Contains("///");

        private void label1_DragDrop(object sender, DragEventArgs e) 
            => ((string[])e.Data.GetData(DataFormats.FileDrop, false)).ToList().ForEach(f => AddComment(f));

        private void label1_DragEnter(object sender, DragEventArgs e)
            => e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Copy : DragDropEffects.None;

        /// <summary>
        /// テキストボックスへの入力動作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTextBoxEnter(object sender, DragEventArgs e)
        {
            // ファイルがドラッグされている場合、カーソルを変更する
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        /// <summary>
        /// テキストボックスへのドラッグアンドドロップ動作
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">引数</param>
        private void OnTextBoxDragDrop(object sender, DragEventArgs e)
        {
            // ドロップされたファイルの一覧を取得
            string[] fileName = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            if (fileName.Length <= 0)
            {
                return;
            }

            // ドロップ先がTextBoxであるかチェック
            TextBox target = sender as TextBox;
            if (target == null)
            {
                return;
            }

            // TextBoxの内容をファイル名に変更
            target.Text = fileName[0];
        }

        /// <summary>
        /// ファイル選択ボタンのクリック動作
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">引数</param>
        private void OnSelectFileButtonClick(object sender, MouseEventArgs e)
        {
            // ダイアログを開く
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                SelectFileTextBox.Text = ofd.FileName;
            }
        }

        /// <summary>
        /// 実行ボタンのクリック動作
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">引数</param>
        private void OnExecuteButtonClick(object sender, MouseEventArgs e)
        {
            // ファイルが存在しない場合
            if (!File.Exists(SelectFileTextBox.Text))
            {
                // ダイアログ表示
                MessageBox.Show("ファイルが存在しません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // C#ファイルでない場合
            if (Path.GetExtension(SelectFileTextBox.Text) != ".cs")
            {
                // ダイアログ表示
                MessageBox.Show("C#のファイルではありません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // コメントの追加
            AddComment(SelectFileTextBox.Text);

            // ダイアログ表示
            MessageBox.Show("コメント追加完了！", "正常終了", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// メニューの開くのクリック動作
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">引数</param>
        private void OnMenuOpenClick(object sender, EventArgs e)
        {
            // ダイアログを開く
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                SelectFileTextBox.Text = ofd.FileName;
            }
        }

        /// <summary>
        /// メニューの実行のクリック動作
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">引数</param>
        private void OnMenuExecuteClick(object sender, EventArgs e)
        {
            // ファイルが存在しない場合
            if (!File.Exists(SelectFileTextBox.Text))
            {
                // ダイアログ表示
                MessageBox.Show("ファイルが存在しません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // C#ファイルでない場合
            if (Path.GetExtension(SelectFileTextBox.Text) != ".cs")
            {
                // ダイアログ表示
                MessageBox.Show("C#のファイルではありません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // コメントの追加
            AddComment(SelectFileTextBox.Text);

            // ダイアログ表示
            MessageBox.Show("コメント追加完了！", "正常終了", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        /// <summary>
        /// メニューのバージョン情報のクリック動作
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">引数</param>
        private void OnMenuVersionClick(object sender, EventArgs e)
        {
            // アセンブリ情報取得
            Assembly mainAssembly = Assembly.GetEntryAssembly();

            // ダイアログ表示
            MessageBox.Show(mainAssembly.GetName().Version.ToString(), "バージョン", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// メニューの終了のクリック動作
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">引数</param>
        private void OnMenuFinishClick(object sender, EventArgs e)
        {
            // 終了
            Close();
        }

        /// <summary>
        /// ダイアログの初期化
        /// </summary>
        private void InitializeSelectDialog()
        {
            string fileName = "default.cs";

            // はじめのファイル名を指定する
            // はじめに「ファイル名」で表示される文字列を指定する
            ofd.FileName = fileName;

            // はじめに表示されるフォルダを指定する
            // 指定しない（空の文字列）の時は、現在のディレクトリが表示される
            ofd.InitialDirectory = @"C:\";

            // [ファイルの種類]に表示される選択肢を指定する
            // 指定しないとすべてのファイルが表示される
            ofd.Filter = "C#ファイル(*.cs)|*.cs";

            //[ファイルの種類]ではじめに選択されるものを指定する
            ofd.FilterIndex = 1;

            // タイトルを設定する
            ofd.Title = "開くファイルを選択してください";

            // ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;
        }
    }
}
